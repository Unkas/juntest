Rails.application.routes.draw do

  devise_for :users
  root 'home#index'

  resources :posts
  resources :users
  get 'users', to: 'users#show'

  get 'api/v1/posts/:id', to: 'posts#show'
  get 'api/v1/posts', to: 'posts#index'
  post 'api/v1/posts', to: 'posts#create'

  post 'api/v1/reports/by_author', to: 'posts#report_by_author'

  get 'raffle' => 'raffle#index'

  get 'basket' => 'basket#index'

end
