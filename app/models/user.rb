class User < ActiveRecord::Base
  has_many :posts
  has_many :comments

  validates :nickname,  presence: true
  validates :email,     presence: true
  #validates :password,  presence: true
  validates :password, length: {minimum: 5, maximum: 120}, on: :update, allow_blank: true

  mount_uploader :avatar, AvatarUploader

  devise :database_authenticatable, :rememberable, :trackable, :validatable
end
