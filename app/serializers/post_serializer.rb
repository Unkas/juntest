class PostSerializer < ActiveModel::Serializer
  attributes :id, :title, :body, :published_at, :author_nickname

  def author_nickname
    unless object.author_id.nil?
      User.find_by(id: object.author_id).nickname
    end
  end

end
