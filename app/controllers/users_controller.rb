class UsersController < ApplicationController

  require 'rmagick'
  include Magick

  def show
    @user = User.find params[:id]
  end

  def update
    user = User.find params[:id]
    user.save

    img = Magick::Image.read(params[:user][:avatar].tempfile.path).first
    #binding.pry
    ilist = Magick::ImageList.new
    mark = Magick::Image.new(img.columns, img.rows)

    gc = Magick::Draw.new
    gc.gravity = Magick::CenterGravity
    gc.pointsize = 32
    gc.font_family = "Helvetica"
    gc.font_weight = Magick::BoldWeight
    gc.stroke = 'none'
    gc.annotate(mark, 0, 0, 0, 0, params[:watermark])


    ilist << mark.shade(true)
    img.composite!(ilist, Magick::CenterGravity, Magick::HardLightCompositeOp)

    img.write 'public/rm_out.png' # save rm picture
    user.avatar = Rails.root.join("public/rm_out.png").open   # use rm picture as uploaded
    #binding.pry
    user.save

    redirect_to :back
  end

end
