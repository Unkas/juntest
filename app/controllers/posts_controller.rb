class PostsController < ApplicationController

# its dangerous option, but I use it once to avoid difficult authenticate system
skip_before_filter :verify_authenticity_token
  def index
    @posts = Post.all.order(published_at: :desc)
    respond_to do |format|
      format.json { render json: @posts }
      response.headers['Posts-count'] = @posts.count.to_s
      #еще требовалось количество страниц передавать - но непонтяно что за величина такая
    end
  end


  def show
    @post = Post.find params[:id]
    respond_to do |format|
      format.json { render json: @post }
    end
  end


  def create
    @post = Post.new
    @post.title = params[:title]
    @post.body = params[:body]
  #поскольку поле author_id связано с аутентификацией (которой нет), пока делаем его устанавливаемым вручную, через параметр
    @post.author_id = params[:author_id]
    if params[:published_at].nil?
      @post.published_at = DateTime.now
    else
      @post.published_at = params[:published_at]
    end
    if @post.save
      respond_to do |format|
        format.json { render json: @post }
      end
    else
      respond_to do |format|
        format.json { render json: @post.errors.messages }
      end
    end
  end



  def report_by_author
    #сделать бэкгроаундный отчет с отправкой по имейлу
    Resque.enqueue(Resqueee, params)
    #binding.pry
    respond_to do |format|
      format.json { render json: @posts }
    end
    #Resque.enqueue(Resqueee, params)


    #ReportMailer.report_delivering(output_array).deliver_now
  end

  def get_nick_by_id user_id
    User.find(user_id).nickname
  end

  def get_email_by_id user_id
    User.find(user_id).email
  end


#  private
#  def post_params
#    params.require(:post).permit(:title, :body, :author_id, :published_at)
#  end

end
