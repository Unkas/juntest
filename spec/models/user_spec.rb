require 'rails_helper'

RSpec.describe User, type: :model do

  it "should can create valid user with avatar" do
  filename =  File.join(Rails.root, 'public/avatar.jpg')
  avatar =   File.open(filename)

  user = User.new( nickname: "John", password: 'coolpass', email: 'test@example.com', avatar: avatar)
  expect(user).to be_valid
  end

end
