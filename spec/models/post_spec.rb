require 'rails_helper'

RSpec.describe Post, type: :model do
  it "should be valid with title and body" do
    post = Post.new(author_id: "1", title: "John", body: "abyrvalg")
    expect(post).to be_valid
  end

end
