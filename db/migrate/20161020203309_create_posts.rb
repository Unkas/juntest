class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.string :body
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_reference :posts, :author, references: :users, index: true
    add_foreign_key :posts, :users, column: :author_id
  end
end
