class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|

      t.string :name
      t.string :code
      t.integer :price

      t.timestamps null: false
    end


    create_table :basket_row do |t|

      t.integer :basket_id
      t.string :product_id
      t.integer :count

      t.timestamps null: false
    end
  end
end
