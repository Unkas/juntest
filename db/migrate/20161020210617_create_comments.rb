class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :body
      t.date :published_at
      t.timestamps null: false
    end
    add_reference :comments, :author, references: :users, index: true
    add_foreign_key :comments, :users, column: :author_id

    add_column :posts, :published_at, :date
  end
end
